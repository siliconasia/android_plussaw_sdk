package com.sawintegtaion;

import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.svc.ui.splash.SVCSplashActivity;

public class SawModule extends ReactContextBaseJavaModule {

    private ReactApplicationContext context;
    public SawModule(ReactApplicationContext reactContext) {

        super(reactContext); //required by React Native
        context = reactContext;
    }

    @NonNull
    @Override
    public String getName() {
        return "SawModule";
    }

    @ReactMethod
    public void showSawActivity() {
//        Toast.makeText(context, "open toast", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(context, SVCSplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
