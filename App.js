import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { render } from 'react-dom';
import { Button, StyleSheet, Text, View , TouchableOpacity, NativeModules, Image } from 'react-native';

const saw = NativeModules.SawModule;

export default class App extends Component{
  render(){
    return(
      <View style={styles.viewStyle}>

<Text  style={styles.titleText}>+SAW Integration with React Native</Text>

        <TouchableOpacity style={styles.button} onPress={()=>
        {saw.showSawActivity();}}>
          <Text style={styles.buttonText}>Procced to +SAW</Text>
        </TouchableOpacity>

        {/* <Image source = {{uri:'https://www.pinclipart.com/picdir/big/537-5374089_react-js-logo-clipart.png'}}
          style = { styles.imageStyle}/> */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: "red",
    padding: 10,
    margin:20,
    borderRadius: 5,
  },
  buttonText: {
    fontSize: 12,
    color: 'white',
    fontWeight: "bold"
  }, 

  titleText: {
    fontSize: 16,
    color: 'black',
    fontWeight: "bold"
  },
  viewStyle: {
    flex:1, 
    alignItems: "center", 
    justifyContent:'center',
    backgroundColor:'white'
  },

  imageStyle: {
    width: 50, 
    height: 50
  },
});
